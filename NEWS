=====================
Version 1.0.0.alpha.4
=====================

- Depend on meson 0.59.0.
- Action Row
  - Drop use-underline property.
  - Stop supporting mnemonics on subtitles.
  - Support markup on the title and subtitle.
  - Fix layout with empty title and subtitle.
  - Don't expand horizontally by default.
  - Fix row titles inside GtkMessageDialog.
- Avatar
  - Crop non-square custom images to fill the avatar.
- Carousel
  - Fix a crash when scrolling in an empty carousel.
- Clamp
  - Set the proper accessible role.
- Combo Row
  - Fix row colors while the popover is open.
- Demo
  - Add a style classes demo.
  - Add about dialog.
  - Add an inspector item to the primary menu.
  - Change appid to org.gnome.Adwaita1.Demo to version it.
  - Add an icon, metainfo and desktop file.
    - Make the desktop file visible for nightly flatpaks.
  - Make non-unique.
  - Tighten flatpak permissions.
  - Various polish.
- Expander Row
  - Drop use-underline property.
  - Rename adw_expander_row_add() to adw_expander_row_add_row().
- Inspector
  - Fix color scheme resetting when opening inspector.
- Preferences Window:
  - Fix a memory leak.
- Status Page
  - Allow setting the icon from a paintable and not just icon name.
  - Set the proper accessible role.
  - Reduce margins with the .compact style class.
- Style Manager
  - Fix styles breaking when setting gtk-application-prefer-dark-theme.
  - Fix crash on systems with xdg-desktop-portal but no settings portal.
  - Fix a memory leak.
- Stylesheet
  - New style classes:
    - .card to have a style similar to a boxed list for a standalone
      widget. Can be combined with .activatable to have hover and active
      states, or can be added to GtkButton to have them automatically.
    - button.opaque to allow custom colored buttons in the style of that
      look like .suggested-action or .destructive-action.
    - tabbar.inline and searchbar.inline - to opt out from using header
      bar colors for AdwTabBar and GtkSearchBar.
  - Boxed lists
    - Rename .content to .boxed-list to match HIG, keep .content as an
      alias.
    - Stop supporting .content with GtkListView as it was broken anyway.
    - Fix focus ring offsets.
  - Sidebars
    - Use the same background as the window.
    - Make .navigation-sidebar handle background, so it's sufficient to
      achieve the proper style.
    - Deprecate the .sidebar style class.
  - Buttons
    - Stop supporting button.flat.suggested-acton and
      button.flat.destructive-action. Special case those styles for
      GtkMessageDialog buttons instead.
    - Support .pill on GtkMenuButton
    - Support .suggested-action and .destructive-action on GtkMenuButton
      and AdwSplitButton.
    - Use toolbar-style buttons for GtkSearchBar.
    - Fix AdwSplitButton styles in high contrast mode.
    - Reduce disabled flat button opacity to make them easier to tell
      apart from the regular state.
  - Popovers
    - Drop popover.combo, make popover.menu handle this case instead.
    - Unify GtkDropDown and AdwComboRow popup styles with menus.
    - Fix a double border with menu radios in high contrast mode.
  - Refresh UI colors.
  - Refresh scrollbar style.
  - Unify progress bar and level bar sizes and styles.
  - Make checks and radios larger.
  - Refresh border radii across the board.
  - Refresh scroll overshoot effect.
  - Remove notebook header background.
  - Export all UI colors and allow overriding them.
  - Use accent color for active drop styles.
  - Ensure proper opacity for the high contrast mode.
  - Simplify recoloring for GtkScale, GtkCheckButton, GtkSwitch,
    GtkColumnView and AdwTabBar.
  - Various toolbar button fixes.
  - Make entry.error and .warning style icons and progress bar too.
  - Fix disabled state on GtkScale, GtkNotebook and GtkSpinButton.
  - Fix GtkMessageDialog paddings.
  - Fix margins on GtkWindowControls icon.
  - Stop removing toolbar.osd rounding in overlays.
  - Remove a GtkGridView override specific to gtk4-icon-browser.
- Tab Bar
  - Fix artifacts when maximizing the window.
  - Fix hover.
  - Handle middle click on button release and not press.
  - Show close button correctly when raising the window and the pointer
    is already over a tab.
- View Switcher
  - Fix screen readers reading page titles 4 times.
- Add API to allow checking libadwaita version in runtime.
- Various fixes and cleanups.
- Translation updates:
  - Finnish
  - Indonesian
  - Occitan
  - Persian
  - Polish
  - Serbian
  - Swedish

===================
Version 1.0.0.alpha.3
===================

- Depend on GTK 4.4.0.
- Add a GtkInspector extension for simulating different system appearance
  settings. It can be disabled with -Dinspector=false.
- Introduce AdwStyleManager for managing color schemes (light/dark) and
  high contrast mode.
  - Support the cross-platform color scheme preference in the
    settings portal.
- Introduce AdwApplication to handle automatic initialization and style
  loading.
- Add adw_is_initialized() function.
- Add AdwSplitButton to have consistent split buttons in toolbars.
- Add AdwButtonContent as an easy way to create buttons with an icon and
  a label inside.
- Remove AdwValueObject.
- Rename AdwEnumValueObject to AdwEnumListItem.
- Avatar:
  - Replace adw_avatar_draw_to_pixbuf() with adw_avatar_draw_to_texture()
    that returns a GdkTexture instead.
    - Remove the size parameter, use the avatar's current size instead.
- Carousel
  - Allow to shrink carousel if children are expanded.
- Clamp
  - Fix measuring with for_size = -1.
- Combo Row:
  - Have a .combo style class.
- Leaflet:
  - Remove hhomogeneous-folded, hhomogeneous-unfolded, vhomogeneous-folded
    and vhomogeneous-unfolded properties. Assume non-homogeneous layout
    when unfolded and homogeneous when folded.
  - Add a single homogeneous property that corresponds to folded state and
    opposite orientation.
  - Remove interpolate-size, assume it's set to true.
  - Set the fold threshold policy to MINIMUM by default.
- Preferences Group:
  - Fix default visibility of the internal list box.
  - Allow markup on title and description.
- Squeezer
  - Fix the child switch threshold in vertical orientation.
  - Add switch-threshold-policy, matching AdwFlap and AdwLeaflet.
  - Add allow-none property, allowing to hide the last child as well.
- Status Page
  - Make icon optional.
- Tab View
  - Fix model updates when page selection changes.
- View Switchers:
  - Remove AdwViewSwitcher:narrow-ellipsize.
  - Remove the policy property from AdwViewSwitcherBar and
    AdwViewSwitcherTitle.
  - Remove the auto policy, applications can use two view
    switchers and an AdwSqueezer instead.
  - Switch to narrow layouts earlier.
- Window and Application Window:
  - Rename the child property to content to avoid the name clash with
    GtkWindow:child.
- Stylesheet:
  - New style classes:
    - headerbar.flat to remove headerbar's background and border.
    - separator.spacer to create spacing in toolbars or header bars.
    - button.pill for prominent standalone buttons, for example on status
      pages.
    - statuspage.compact to make AdwStatusPage suitable for contexts such
      as sidebars.
    - .numeric as an easy way to enable tabular figures in a label.
  - Use flat buttons by default in header bars and action bars, matching
    existing .toolbar.
    - .raised style class on GtkButton, GtkMenuButton, or AdwSplitButton
      can be used to use the regular style instead.
  - Make window controls filled instead, leave the clickable area larger
    than the visible buttons.
  - Fix broken styles when .content style class is used together with
    GtkListBox:show-separators set to true.
  - Remove @content_view_bg and @text_view_bg colors.
  - Remove .content-view style class.
  - Add previously removed public colors @theme_selected_bg/fg_color and
    the backdrop colors as compatibility aliases. Applications shouldn't
    use them in new code regardless.
  - Use stripes for header bars in .devel windows instead of cogs to make
    it work better with split header bars and flat header bars.
  - Make .osd more visible in dark variant.
  - Make borders more visible in dark variant.
  - Fix padding on buttons inside popovers inside header bars.
  - Fix double focus rings on labels.
- Implement get_request_mode() and compute_expand() where appropriate.
- Various fixes and cleanups.
- Translation updates:
  - Brazilian Portuguese
  - Czech
  - Danish
  - Galician
  - Lithuanian
  - Korean
  - Portuguese
  - Serbian
  - Ukrainian

=====================
Version 1.0.0-alpha.2
=====================

- Stylesheet:
  - Large redesign to make it flat and recolorable.
  - Replace @theme_suggested_bg_color by @accent_bg_color and
    @accent_color.
  - Replace @theme_suggested_fg_color by @accent_fg_color.
  - Add @destructive_bg_color, @destructive_fg_color and
    @destructive_color.
  - Add the .accent style class to give the accent color to labels.
  - Add the palette colors in the form @hue_n, with hue being blue,
    green, yellow, orange, red, purple, brown, light and dark, and n
    being a darkness level from 1 to 5.
- View Switcher:
  - Add the AdwViewStack widget to represent views.
  - Use AdwViewStack instead of GtkStack.
  - Display a badge on buttons to display the pages' value from the
    AdwViewStack:badge-number property.
  - Keep displaying needs-attention when active.
- Preferences Page:
  - Add the name property.
- Preferences Window:
  - Add the visible-child and visible-child-name properties.
- Leaflet and Flap:
  - Add the AdwFoldThresholdPolicy enumeration.
  - Add the fold-threshold-policy property to determine the size at
     which the leaflet or flap should fold.
- Leaflet:
  - Fix a crash by NULL-checking a pointer before dereferencing it when
    there is no children.
  - Annotate the values of the visible-child and visible-child-name
    properties as nullable in their accessors.
- Action Row and Expander Row:
  - Annotate the value of the icon-name property as nullable in its
    accessors.
- Tab View:
  - Fix updating the model at the right time after attaching pages.
  - Fix emitting notify::selected-page after the model has been
    completely updated.
  - Prevent pages from receiving pointer events during drag and drop.
- Combo Row:
  - Fix subtitles when the model is empty and when using expressions.
- Carousel:
  - Fix a crash with 2 overlapping animations.
  - Fix a crash when scrolling when there is no children.
- Avatar:
  - Fix memory leaks in adw_avatar_draw_to_pixbuf().
  - Fix a memory leak in the avatar demo.
- Fix crashes by freeing shaders at the right time.
- Specify the translation domain in UI files to avoid leaving them
  unlocalized.
- Fix cross-compilation with -Dgtk_doc=true.
- Stop accepting NULL for most string properties, use the empty string
  instead.
- Translation updates:
  - German
  - Indonesian
  - Chinese (China)

=====================
Version 1.0.0-alpha.1
=====================

- First libadwaita 1 alpha.
- Check the migration guide in the documentation to port from libhandy
  to it.
